using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
     Rigidbody rb;
    public LayerMask whatIsEnemies;

    [Range(0f, 1f)]
    public float bounciness;
    public bool useGravity;

    public int maxCollisions;
    public float maxLifetime;

    int collisions;
    PhysicMaterial physics_mat;

    float damage = 0;
    string attacker;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        physics_mat = new PhysicMaterial();
        physics_mat.bounciness = bounciness;
        physics_mat.frictionCombine = PhysicMaterialCombine.Minimum;
        physics_mat.bounceCombine = PhysicMaterialCombine.Maximum;
        GetComponent<SphereCollider>().material = physics_mat;
        rb.useGravity = useGravity;
    }

    private void Update()
    {

        if (collisions > maxCollisions) Invoke("Delay", 0.05f);

        maxLifetime -= Time.deltaTime;
        if (maxLifetime <= 0) Invoke("Delay", 0.05f);
    }

    private void Delay()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Bullet")) return;
        collisions++;
    }

    public float getDamage()
    {
        return damage;
    }
    public void setDamage(float damage)
    {
        this.damage = damage;
    }
    public void setAttacker(string attacker)
    {
        this.attacker = attacker;
    }
    public string getAttacker()
    {
        return attacker;
    }
}
