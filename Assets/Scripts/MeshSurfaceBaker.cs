using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class MeshSurfaceBaker : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField]
    NavMeshSurface navMeshSurfaces;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        navMeshSurfaces.BuildNavMesh();
    }
}
