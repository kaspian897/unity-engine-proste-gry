using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Blocks : MonoBehaviour
{
    // Start is called before the first frame update
     NavMeshSurface surface;
    
    void Start()
    {
        surface = GameObject.Find("Surface").GetComponent<NavMeshSurface>();
        //settings.
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Bullet"))
        {
            gameObject.SetActive(false);
            surface.BuildNavMesh();
            Destroy(gameObject);
            // Bullet bullet = collision.collider.GetComponent<Bullet>();
            // if (bullet.getAttacker().CompareTo("Player") == 0)
            // {
            //    TakeDamage(bullet.getDamage());
            //}
        }

    }
}
