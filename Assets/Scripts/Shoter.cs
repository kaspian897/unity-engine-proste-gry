using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Shoter : MonoBehaviour
{

    public GameObject bullet_1;
    public GameObject bullet_2;
     GameObject bullet;

    public float shootForce, upwardForce;

    public float timeBetweenShooting, reloadTime, timeBetweenShots;
    public int magazineSize;
    public bool allowButtonHold;

    int bulletsLeft;

    public Rigidbody playerRb;

    bool shooting, readyToShoot, reloading;

    public Camera fpsCam;
    public Transform attackPoint;

    public TextMeshProUGUI ammunitionDisplay;

    bool allowInvoke = true;

    public float damge=100;
    public int ammoPack = 320;

    int bulletType = 1;
    int bulletShot;
    public void setBulletType(int type)
    {
        bulletType = type;
    }
    private void Awake()
    {
        bulletsLeft = magazineSize;
        readyToShoot = true;
    }

    int left;
    bool bulletChange = false;
    private void Update()
    {

        if (allowButtonHold) shooting = Input.GetKey(KeyCode.Mouse0);
        else shooting = Input.GetKeyDown(KeyCode.Mouse0);

        //Reload
        if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !reloading) Reload();

        if (readyToShoot && shooting && !reloading && bulletsLeft <= 0) Reload();

        //Shoot
        if (readyToShoot && shooting && !reloading && bulletsLeft > 0)
        {
            bulletShot = 0;
            Shoot();
        }

        ///////////////////

        if (ammunitionDisplay != null)
        {
            if (bulletType == 1)
            {
                ammunitionDisplay.SetText(ammoPack + " / " + bulletsLeft);
                bullet = bullet_1;
                timeBetweenShots = 0.1f;
                if (bulletChange)
                {
                    bulletsLeft = left;
                    bulletChange = false;
                }
                allowButtonHold = false;
            }
            else if(bulletType==2)
            {
                ammunitionDisplay.SetText("\u221E");
                bullet = bullet_2;
                timeBetweenShots = 0.08f;
                if (!bulletChange)
                {
                    left = bulletsLeft;
                    bulletChange = true;
                }
                bulletsLeft = int.MaxValue;
                allowButtonHold = true;
            }
        }
            
    }


    private void Shoot()
    {
        readyToShoot = false;

        Ray ray = fpsCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)); 
        RaycastHit hit;

        Vector3 targetPoint;
        if (Physics.Raycast(ray, out hit))
            targetPoint = hit.point;
        else
            targetPoint = ray.GetPoint(75); 

        
        Vector3 direction = targetPoint - attackPoint.position;

        GameObject currentBullet = Instantiate(bullet, attackPoint.position, Quaternion.identity); 

        currentBullet.GetComponent<Bullet>().setAttacker("Player");
        currentBullet.GetComponent<Bullet>().setDamage(this.damge);
        currentBullet.transform.forward = direction.normalized;


        currentBullet.GetComponent<Rigidbody>().AddForce(direction.normalized * shootForce, ForceMode.Impulse);
        currentBullet.GetComponent<Rigidbody>().AddForce(fpsCam.transform.up * upwardForce, ForceMode.Impulse);



        bulletsLeft--;
        bulletShot++;

        if (allowInvoke)
        {
            Invoke("ResetShot", timeBetweenShooting);
            allowInvoke = false;

            
        }


        if (bulletShot < 1&&bulletsLeft > 0)
            Invoke("Shoot", timeBetweenShots);
    }
    private void ResetShot()
    {

        readyToShoot = true;
        allowInvoke = true;
    }

    private void Reload()
    {
        reloading = true;
        if (ammoPack>0)
        {
            Invoke("ReloadMagazine", reloadTime);
        }
        
    }
    private void ReloadMagazine()
    {
        
        int reloadBullet = magazineSize;
        if(ammoPack< magazineSize)
        {
            reloadBullet = ammoPack;
        }


        ammoPack -= reloadBullet- bulletsLeft;
        bulletsLeft = reloadBullet;
        reloading = false;
    }
}
