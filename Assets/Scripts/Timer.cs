using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Timer : MonoBehaviour
{
    // Start is called before the first frame update
    float timer = 0.0f;
    float startTime = 0.0f;
    void Start()
    {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        timer = Time.time - startTime;
        float minutes = Mathf.Floor(timer / 60);
        float seconds = Mathf.RoundToInt(timer % 60);

        string write = "Time: ";
        if (minutes < 10)
        {
            write += "0" + minutes.ToString();
        }
        else
        {
            write += minutes.ToString();
        }
        write += ":";
        if (seconds < 10)
        {
            write += "0" + Mathf.RoundToInt(seconds).ToString();
        }
        else
        {
            write += Mathf.RoundToInt(seconds).ToString();
        }
        GetComponent<TextMeshProUGUI>().text = write;
    }
}

