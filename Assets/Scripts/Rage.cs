using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class Rage : MonoBehaviour
{
    // Start is called before the first frame update
    public float rageBarWidth = 50;
    public TextMeshProUGUI rageText;
    public Image rageBar;
    public TextMeshProUGUI killedText;
    public int rage=0;
    public float lifetimeOneRage=2;
    public float liftime=10;
    float startTime;

    public int rageBullet=5;
    public Image bullet1;
    public Image bullet2;
    int killed = 0;

    public Shoter shoter;
    void Start()
    {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        float timer = Time.time - startTime;
        float difrence = (lifetimeOneRage - timer);
        if (difrence <= 0)
        {
            startTime = Time.time;
            difrence = 0;
            rage--;
            if (rage <=0)
            {
                rage = 0;
            }
            if (rage >= rageBullet)
            {
                bullet2.enabled = true;
                bullet1.enabled = false;
                shoter.setBulletType(2);
            }
            else
            {
                bullet2.enabled = false;
                bullet1.enabled = true;
                shoter.setBulletType(1);
            }

        }
        float bar= (difrence / lifetimeOneRage)* rageBarWidth;
        if (rage <= 0)
        {
            bar = 0;
        }

        rageBar.rectTransform.sizeDelta = new Vector2(bar, rageBar.rectTransform.sizeDelta.y);
        rageText.text = rage.ToString();


    }
    public void addRage()
    {
        rage++;
        rageBar.rectTransform.sizeDelta=new Vector2(rageBarWidth,rageBar.rectTransform.sizeDelta.y);
        startTime = Time.time;
        rageText.text = rage.ToString();
        if (rage >= rageBullet)
        {
            bullet2.enabled = true;
            bullet1.enabled = false;
            shoter.setBulletType(2);
        }
        else
        {
            bullet2.enabled = false;
            bullet1.enabled = true;
            shoter.setBulletType(1);
        }
    }
    public void addKill()
    {
        killed++;
        killedText.text = killed.ToString();
    }
    public int getKills()
    {
        return killed;
    }
}
