using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class WiningCondition : MonoBehaviour
{
    // Start is called before the first frame update
    int minimumKills;
    public GameObject canvas;
    void Start()
    {
        minimumKills = GameObject.FindObjectsOfType<EnemyAI>().Length+GameObject.FindObjectsOfType<EnemyAIFly>().Length; 
    }

    // Update is called once per frame
    void Update()
    {
        if(GetComponent<Rage>().getKills() >= minimumKills)
        {
            canvas.SetActive(true);
        }
    }
}
