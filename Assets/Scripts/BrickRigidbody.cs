using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickRigidbody : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody>().isKinematic = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            Invoke("offKinematic", 0.5f);
        }
    }
    void offKinematic()
    {
        GetComponent<Rigidbody>().isKinematic = false;
    }
}
