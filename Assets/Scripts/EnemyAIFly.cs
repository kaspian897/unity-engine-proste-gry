using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyAIFly : MonoBehaviour
{
    NavMeshAgent agent;

    public Transform player;

    public LayerMask whatIsGround, whatIsPlayer;

    public float health;

    Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;

    public float timeBetweenAttacks;
    bool alreadyAttacked;
    public GameObject bullet;

    public float sightRange, attackRange;
    bool playerInSightRange, playerInAttackRange;

    public float damage = 10;
    bool isDead = false;
    private void Awake()
    {
        player = GameObject.Find("Player").transform;
        agent = GetComponent<NavMeshAgent>();
    }
    private void Start()
    {
        walkPointSet = false;
        agent = GetComponent<NavMeshAgent>();


    }
    private void Update()
    {
        if (transform.position.y < -15)
        {
            TakeDamage(100000);
        }


        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

        if (!playerInSightRange && !playerInAttackRange) Patroling();
        if (playerInSightRange && !playerInAttackRange) GoToPlayer();
        if (playerInAttackRange && playerInSightRange)
        {
            AttackPlayer();

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Bullet"))
        {
            Bullet bullet = collision.collider.GetComponent<Bullet>();
            if (bullet.getAttacker().CompareTo("Player")==0)
            {
                TakeDamage(bullet.getDamage());
            }
        }
       
    }

    private void Patroling()
    {

        if (!walkPointSet) RandomPoint();

        if (walkPointSet)
        {
            agent.SetDestination(walkPoint);
        }

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;
    }
    private void RandomPoint()
    {

        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
            walkPointSet = true;
    }

    private void GoToPlayer()
    {
        agent.SetDestination(player.position);
    }

    private void AttackPlayer()
    {
        agent.SetDestination(transform.position);

        transform.LookAt(player);

        if (!alreadyAttacked)
        {

            GameObject b = Instantiate(bullet, transform.position + new Vector3(0, 1, 0)+ transform.forward.normalized*1, Quaternion.identity);
            b.GetComponent<Bullet>().setDamage(damage);
            b.GetComponent<Bullet>().setAttacker("Enemy");
            Rigidbody rb = b.GetComponent<Rigidbody>();
            
            rb.AddForce(transform.forward* 32f, ForceMode.Impulse);

            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), timeBetweenAttacks);
        }
    }
    private void ResetAttack()
    {
        alreadyAttacked = false;
    }

    public void TakeDamage(float damage)
    {
        health -= damage;

        if (health <= 0&&!isDead)
        {
            isDead = true;
            GameObject.Find("Player").GetComponent<Rage>().addRage();
            GameObject.Find("Player").GetComponent<Rage>().addKill();
            GetComponent<Animator>().Play("Die");
            Invoke(nameof(DestroyEnemy), 1f);
        }
    }
    private void DestroyEnemy()
    {
        
        Destroy(gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }
}
