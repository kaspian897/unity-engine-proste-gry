using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    

    public float normalSpeed = 900f;
    public float sprintSpeed = 2000f;
 

    public float sensitivity = 1f;
    public float yRotSpeed=10;

    public Camera cam;

    public GameObject groudCheck;
    public float jumpHeight;
    public float jumpSpeed = 10;
    public int maxJumpCount = 2;
    int jumpCounter = 0;

    Rigidbody rb;

    public float health = 200;
    public float actualHealth = 200;
    public TMP_Text healthText;
    public Image healthImage;
    Vector2 size;

    static int death = 0;
    public TextMeshProUGUI deathCounter;
    public GameObject canvas;
    bool isdead;
    // Start is called before the first frame update
    void Start()
    {
        isdead = false;
        deathCounter.text = death.ToString();
        actualHealth = health;
        rb = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        size = healthImage.rectTransform.sizeDelta;
    }


    private void Update()
    {
        if (transform.position.y<-15)
        {
            canvas.SetActive(true);
            Invoke("gameOver", 1f);

        }
        Vector2 xMov = new Vector2(Input.GetAxisRaw("Horizontal") * transform.right.x, Input.GetAxisRaw("Horizontal") * transform.right.z);
        Vector2 zMov = new Vector2(Input.GetAxisRaw("Vertical") * transform.forward.x, Input.GetAxisRaw("Vertical") * transform.forward.z);

        float speed = normalSpeed;
        if (Input.GetKey(KeyCode.LeftShift))
        {
            speed = sprintSpeed;
        }
        Vector2 velocity = (xMov + zMov).normalized * speed;

        // Rotation
        float yRot = Input.GetAxisRaw("Mouse X") * sensitivity* yRotSpeed;
        rb.rotation *= Quaternion.Euler(0, yRot, 0);

        float xRot = Input.GetAxisRaw("Mouse Y") * sensitivity;
        float x_rot = cam.transform.rotation.eulerAngles.x;
        x_rot -= xRot;

        float camEulerAngleX = cam.transform.localEulerAngles.x;

        camEulerAngleX -= xRot * sensitivity;

        if (camEulerAngleX < 180)
        {
            camEulerAngleX += 360;
        }

        camEulerAngleX = Mathf.Clamp(camEulerAngleX, 270, 450);

        cam.transform.localEulerAngles = new Vector3(camEulerAngleX, 0, 0);


        //Jumping
        bool isGrounded = Physics.BoxCastAll(groudCheck.transform.position, new Vector3(0.4f, 0.3f, 0.4f), Vector3.down, Quaternion.identity, .1f).Length > 1;

        if (isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            jumpCounter=1;
            Jump();
        }
        else if(jumpCounter>=1&&jumpCounter < maxJumpCount && Input.GetKeyDown(KeyCode.Space))
        {
            jumpCounter++;
            Jump();
        }


        rb.velocity = new Vector3(velocity.x, rb.velocity.y, velocity.y);
    }
    void Jump()
    {
        rb.velocity = new Vector3(rb.velocity.x,jumpSpeed, rb.velocity.z);
    }

    private void OnCollisionEnter(Collision collision)
    {
       if (collision.collider.tag == "Bullet")
       {
            Bullet bullet = collision.collider.GetComponent<Bullet>();
            if (bullet.getAttacker().CompareTo("Enemy") == 0)
            {


                actualHealth -= bullet.getDamage();

                if (actualHealth <= 0)
                {
                    actualHealth = 0;
                    canvas.SetActive(true);
                    Invoke("gameOver",1f);
                    //gameOver();
                }
                //healthText.text = actualHealth.ToString();
                float x = actualHealth / health * size.x;
                healthImage.rectTransform.sizeDelta = new Vector2(x, size.y);
            }

       }
    }
    public void gameOver()
    {
        if (!isdead)
        {
            death++;
            isdead = true;
            deathCounter.text = death.ToString();
            SceneManager.LoadScene("SampleScene");
        }
    }
}
